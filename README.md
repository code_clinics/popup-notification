# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the Popup Notification module
* 0.0.1
* More Tutorial (https://bitbucket.org/code_clinics)

### How do I get set up? ###

* You can pull or download zip file of this module
* In system configuration you need to enable and make necessary changes to make it working
* I have used Fancy-box for popup and added Jquey libray you can remove you have already loaded it
* You can set cookie time for popup from admin panel configuration of module
* PopUp will be blank you can add image/content/template/newsletter block as per your requiremnt


You can see module configuration here
![popup_notification.png](https://bitbucket.org/repo/M8KdL9/images/3841182353-popup_notification.png)

In the front-end you can see pop like below screen. you can add content and modify its layout.
![popup front.png](https://bitbucket.org/repo/M8KdL9/images/588771134-popup%20front.png) 
### Contribution guidelines ###

* You can contribute here for more stable product

### Support? ###

* Please create issue here or write me on info@codelinics.com